<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210906220325 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE billetera (id INT AUTO_INCREMENT NOT NULL, id_usuario_id INT NOT NULL, saldo DOUBLE PRECISION NOT NULL, creacion DATETIME NOT NULL, modificado DATETIME NOT NULL, INDEX IDX_B437632B7EB2C349 (id_usuario_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE token (id INT AUTO_INCREMENT NOT NULL, id_transaccion_id INT NOT NULL, UNIQUE INDEX UNIQ_5F37A13BB0DFD449 (id_transaccion_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE transaccion (id INT AUTO_INCREMENT NOT NULL, id_billetera_remitente_id INT NOT NULL, id_billetera_destino_id INT NOT NULL, valor DOUBLE PRECISION NOT NULL, descripcion VARCHAR(255) DEFAULT NULL, estado INT NOT NULL, creacion DATETIME NOT NULL, INDEX IDX_BFF96AF7CB837E13 (id_billetera_remitente_id), INDEX IDX_BFF96AF7C0CC56F (id_billetera_destino_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE billetera ADD CONSTRAINT FK_B437632B7EB2C349 FOREIGN KEY (id_usuario_id) REFERENCES usuario (id)');
        $this->addSql('ALTER TABLE token ADD CONSTRAINT FK_5F37A13BB0DFD449 FOREIGN KEY (id_transaccion_id) REFERENCES transaccion (id)');
        $this->addSql('ALTER TABLE transaccion ADD CONSTRAINT FK_BFF96AF7CB837E13 FOREIGN KEY (id_billetera_remitente_id) REFERENCES billetera (id)');
        $this->addSql('ALTER TABLE transaccion ADD CONSTRAINT FK_BFF96AF7C0CC56F FOREIGN KEY (id_billetera_destino_id) REFERENCES billetera (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE transaccion DROP FOREIGN KEY FK_BFF96AF7CB837E13');
        $this->addSql('ALTER TABLE transaccion DROP FOREIGN KEY FK_BFF96AF7C0CC56F');
        $this->addSql('ALTER TABLE token DROP FOREIGN KEY FK_5F37A13BB0DFD449');
        $this->addSql('DROP TABLE billetera');
        $this->addSql('DROP TABLE token');
        $this->addSql('DROP TABLE transaccion');
    }
}
