<?php

namespace App\Controller;

use App\Entity\Usuario;
use App\Repository\UsuarioRepository;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Loader\Configurator\RoutingConfigurator;


class UsuarioController extends AbstractController
{

    private $usuarioRepository;

    public function __construct(UsuarioRepository $usuarioRepository)
    {
        $this->usuarioRepository = $usuarioRepository;
    }

    /**
     * @Route("/usuario", methods={"GET"}, name="usuario")
     */
    public function index(\Swift_Mailer $mailer): Response
    {
        // try
        // {
            //var_dump($data);die;
            return $this->json([
                'message' => 'Welcome to your new controller!',
                'path' => 'src/Controller/UsuarioController.php',
            ]);
        // }
        // catch (Exception $e)
        // {
        //     return $this->json([
        //         'message' => 'false!',
        //         'path' => 'src/Controller/UsuarioController.php',
        //     ]);
        // }
    }

    /**
     * @Route("/usuario", name="add_usuario", methods={"POST"})
     */
    public function add(Request $request)
    {
        $usuario = new Usuario();
        $usuario->setDocumento($request->request->get('documento'));
        $usuario->setNombres($request->request->get('nombres'));
        $usuario->setEmail($request->request->get('email'));
        $usuario->setCelular($request->request->get('celular'));

        try
        {
            $em = $this->getDoctrine()->getManager();
            $em->persist($usuario);
            $em->flush();

            if($em)
            {
                
                $data = $this->forward('app.billetera_controller:add', ['usuario' => $usuario]);

                return $data;
            }
            else {
                return false;
            }
        }
        catch (Exception $e)
        {
            return new JsonResponse([
                'success' => false,
                'cod_error' => '04',
                'message_error' => 'Ha ocurrido un problema registrando el usuario',
                'data' => false
            ], Response::HTTP_OK);
        }
        
    }




    
}
