<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MailerController extends AbstractController
{
    /**
     * @Route("/mailer", name="mailer")
     */
    public function index(): Response
    {
        return $this->json([
            'message' => 'Welcome to your new controller!',
            'path' => 'src/Controller/MailerController.php',
        ]);
    }

    public function enviarCorreo(array $data, \Swift_Mailer $mailer)
    {
        $message = (new \Swift_Message($data[0]['asunto']))
            ->setFrom('ghensysepayco@gmail.com')
            ->setTo('ghensysepayco@gmail.com')
            ->setBody( $data[0]['message'],'text/html');

        $mailer->send($message);

        return $this->json([
            'message' => 'Welcome to your new controller!',
            'path' => 'src/Controller/UsuarioController.php',
        ]);
    }
}
