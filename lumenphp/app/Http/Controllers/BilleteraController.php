<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Http\Request;

class BilleteraController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @param Request $request
     * @param string $documento
     * @param string $celular
     * @return Response
     */
    public function consultarSaldo(Request $request, $documento, $celular)
    {
        $client = new Client();
        $response = $client->request('GET', 'http://backendrest/index.php/billetera/'.$documento.'/'.$celular);

        return json_decode($response->getBody(), true);
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function recargarBilletera(Request $request)
    {
        $client = new Client();
        $response = $client->request('POST', 'http://backendrest/index.php/billetera/recargar', [
            'form_params' => [
                'documento' => $request->input('documento'),
                'celular' => $request->input('celular'),
                'valor' => $request->input('valor')
            ]
        ]);

        return json_decode($response->getBody(), true);
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function pagar(Request $request)
    {
        $client = new Client();
        $response = $client->request('POST', 'http://backendrest/index.php/billetera/pagar', [
            'form_params' => [
                'documento_emisor' => $request->input('documento_emisor'),
                'celular_emisor' => $request->input('celular_emisor'),
                'documento_receptor' => $request->input('documento_receptor'),
                'celular_receptor' => $request->input('celular_receptor'),
                'valor' => $request->input('valor')
            ]
        ]);

        return json_decode($response->getBody(), true);
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function confirmarPago(Request $request)
    {
        $client = new Client();
        $response = $client->request('POST', 'http://backendrest/index.php/billetera/confirmar_pago', [
            'form_params' => [
                'token' => $request->input('token'),
                'id_transaccion' => $request->input('id_transaccion')
            ]
        ]);

        return json_decode($response->getBody(), true);
    }
}
