<?php

namespace App\Controller;

use Exception;
use App\Entity\Billetera;
use App\Entity\Usuario;
use App\Repository\BilleteraRepository;
use App\Repository\UsuarioRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class BilleteraController extends AbstractController
{
    private $billeteraRepository;

    private $usuarioRepository;

    public function __construct(BilleteraRepository $billeteraRepository, UsuarioRepository $usuarioRepository)
    {
        $this->billeteraRepository = $billeteraRepository;
        $this->usuarioRepository = $usuarioRepository;
    }

    public function add(Usuario $usuario): Response
    {
        $billetera = new Billetera();
        $billetera->setIdUsuario($usuario);
        $billetera->setSaldo(0);
        $billetera->setCreacion(new \DateTime());
        $billetera->setModificado(new \DateTime());

        try {

            $em = $this->getDoctrine()->getManager();
            $em->persist($billetera);
            $em->flush();

            return new JsonResponse([
                'success' => true,
                'cod_error' => '00',
                'message_error' => false,
                'data' => [
                    'message' => 'Billetera generada satisfactoriamente.'
                ]
            ], Response::HTTP_OK);
        }
        catch (Exception $e) {

            return new JsonResponse([
                'success' => false,
                'cod_error' => '04',
                'message_error' => 'Ha ocurrido un problema generando la billetera',
                'data' => false
            ], Response::HTTP_OK);
        }
    }

    /**
     * @Route("/billetera/recargar", methods={"POST"})
     */
    public function recargarBilletera(Request $request): Response
    {
        $cod_biletera_recarga = $this->billeteraRepository->find(2);
        $request->request->get('valor');
        $tipo_transaccion = "recarga";

        try {

            $usuario = $this->usuarioRepository->findBy(array(
                'documento' => $request->request->get('documento'),
                'celular' => $request->request->get('celular')
            ));

            $billetera = $this->billeteraRepository->find($usuario[0]->getBilleteras()[0]->getId());
            $billetera->setSaldo($billetera->getSaldo()+$request->request->get('valor'));
            $em = $this->getDoctrine()->getManager();
            $em->flush();

            if($em) {
                $response = $this->forward('app.transaccion_controller:add', [
                    'data' => array([
                        'remitente' => $cod_biletera_recarga,
                        'destino' => $billetera,
                        'valor' => $request->request->get('valor'),
                        'tipo_transaccion' => $tipo_transaccion,
                        'mensaje' => 'Su recarga por un valor de '.$request->request->get('valor').' a sido exitosa.',
                        'estado' => 1
                    ])
                ]);

                return $response;
            }
            else {
                return false;
            }
        }
        catch (Exception $e) {

            return new JsonResponse([
                'success' => false,
                'cod_error' => '04',
                'message_error' => 'Ha ocurrido un problema actualizando el saldo',
                'data' => false
            ], Response::HTTP_OK);
        }
    }

    /**
     * @Route("/billetera/pagar", methods={"POST"})
     */
    public function pagar(Request $request): Response
    {
        $session = $request->getSession();
        $tipo_transaccion = 'pago';
        //datos de la sesion iniciada
        $emisor = $this->usuarioRepository->findBy(array(
            'documento' => $request->request->get('documento_emisor'),
            'celular' => $request->request->get('celular_emisor')
        ));
        $session->set('id', $emisor[0]->getId());
        $session->set('nombres', $emisor[0]->getNombres());

        $billetera_emisor = $this->billeteraRepository->find($emisor[0]->getBilleteras()[0]->getId());
        $session->set('billetera_emisor', $billetera_emisor);

        //receptor
        $receptor = $this->usuarioRepository->findBy(array(
            'documento' => $request->request->get('documento_receptor'),
            'celular' => $request->request->get('celular_receptor')
        ));
        $billetera_receptor = $this->billeteraRepository->find($receptor[0]->getBilleteras()[0]->getId());
        $session->set('billetera_receptor', $billetera_receptor);

        //validar saldo
        $saldo_billetera = $emisor[0]->getBilleteras()[0]->getSaldo();

        //Saldo mayor que valor de transaccion
        if($request->request->get('valor') <= $saldo_billetera) {
            $fecha = new \DateTime();
            $string = $fecha->getTimestamp()+$session->get('id');
            $token = substr($string,-6);
            $encrypt = md5($token);

            $session->set('valor', $request->request->get('valor'));

            $response = $this->forward('app.transaccion_controller:add', [
                'data' => array([
                    'remitente' => $billetera_emisor,
                    'destino' => $billetera_receptor,
                    'valor' => $request->request->get('valor'),
                    'tipo_transaccion' => $tipo_transaccion,
                    'mensaje' => 'Esta realizando un pago por un valor de '.$request->request->get('valor').' a '.$receptor[0]->getNombres(),
                    'estado' => 2 //2 pendiente
                ])
            ]);

            $data = json_decode($response->getContent(),true);

            $session->set('id_transaccion', $data['data']['codigo_transaccion']);
            //registrar token
            $response_token = $this->forward('app.token_controller:add', [
                'data' => array([
                    'id_transaccion' => $data['data']['codigo_transaccion'],
                    'token' => $encrypt,
                ])
            ]);

            $response_token = json_decode($response_token->getContent(), true);

            if($response_token['success'])
            {
                $this->forward('app.mailer_controller:enviarCorreo', [
                    'data' => array([
                        'asunto' => 'Token de confirmación',
                        'message' => 'Su codigo de confirmación para la transacción (Pago a '.$receptor[0]->getNombres().' por un valor de '.$request->request->get('valor').') es: '.$token,
                    ])
                ]);

                return new JsonResponse([
                    'success' => true,
                    'cod_error' => '00',
                    'message_error' => false,
                    'data' => [
                        'message' => 'Verifique su correo para obtener el token de confirmación.'
                    ]
                ], Response::HTTP_OK);
            }
        }
        //saldo menor que valor de transaccion
        else {
            return new JsonResponse([
                'success' => false,
                'cod_error' => '01',
                'message_error' => 'No posee saldo suficiente para realizar esta transacción.',
            ], Response::HTTP_OK);
        }
    }

    /**
     * @Route("/billetera/confirmar_pago", methods={"POST"})
     */
    public function confirmarPago(Request $request)
    {
        $session = $request->getSession();

        if (!is_null($session->get('id'))) {
            $encrypt = md5($request->request->get('token'));
            $id_transaccion = $session->get('id_transaccion');

            //validar token
            $response_token = $this->forward('app.token_controller:validar', [
                'data' => array([
                    'id_transaccion' => $session->get('id_transaccion'),
                    'token' => $encrypt,
                ])
            ]);

            $confirmacion = json_decode($response_token->getContent(), true);

            if ($confirmacion['success']) {
                try {

                    $emisor = $this->billeteraRepository->find($session->get('billetera_emisor'));
                    $emisor->setSaldo($emisor->getSaldo()-$session->get('valor'));

                    $receptor = $this->billeteraRepository->find($session->get('billetera_receptor'));
                    $receptor->setSaldo($receptor->getSaldo()+$session->get('valor'));

                    $em = $this->getDoctrine()->getManager();
                    $em->flush();

                    $this->forward('app.mailer_controller:enviarCorreo', [
                        'data' => array([
                            'asunto' => 'Transacción Satisfactoria',
                            'message' => 'Su transacción con el codigo '.$session->get('id_transaccion').' ha sido satisfactoria.',
                        ])
                    ]);

                    return new JsonResponse([
                        'success' => true,
                        'cod_error' => '00',
                        'message_error' => false,
                        'data' => [
                            'message' => 'SU transacción se ha realizado con exito.'
                        ]
                    ], Response::HTTP_OK);
                } catch (Exception $e) {
                    return $response_token;
                }
            }
            else {
                return $response_token;
            }
        }
    }

    /**
     * @Route("/billetera/{documento}/{celular}", methods={"GET"})
     */
    public function consultarSaldo(Request $request, $documento, $celular): Response
    {
        $usuario = $this->usuarioRepository->findBy(array(
            'documento' => $documento,
            'celular' => $celular
        ));

        if ($usuario) {
            $saldo = $usuario[0]->getBilleteras()[0]->getSaldo();
            return new JsonResponse([
                'success' => true,
                'cod_error' => '00',
                'message_error' => false,
                'data' => [
                    'saldo' => $saldo
                ]
            ], Response::HTTP_OK);
        }
        else{
            return new JsonResponse([
                'success' => false,
                'cod_error' => '04',
                'message_error' => 'Los datos no coinciden, por favor verifique e intentelo nuevamente.',
                'data' => false
            ], Response::HTTP_OK);
        }
    }
}
