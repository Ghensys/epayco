<?php

namespace App\Entity;

use App\Repository\BilleteraRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=BilleteraRepository::class)
 */
class Billetera
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Usuario::class, inversedBy="billeteras")
     * @ORM\JoinColumn(nullable=false)
     */
    private $id_usuario;

    /**
     * @ORM\Column(type="float")
     */
    private $saldo;

    /**
     * @ORM\Column(type="datetime")
     */
    private $creacion;

    /**
     * @ORM\Column(type="datetime")
     */
    private $modificado;

    /**
     * @ORM\OneToMany(targetEntity=Transaccion::class, mappedBy="id_billetera_remitente")
     */
    private $transaccion_remitente;

    public function __construct()
    {
        $this->transaccion_remitente = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdUsuario(): ?Usuario
    {
        return $this->id_usuario;
    }

    public function setIdUsuario(?Usuario $id_usuario): self
    {
        $this->id_usuario = $id_usuario;

        return $this;
    }

    public function getSaldo(): ?float
    {
        return $this->saldo;
    }

    public function setSaldo(float $saldo): self
    {
        $this->saldo = $saldo;

        return $this;
    }

    public function getCreacion(): ?\DateTimeInterface
    {
        return $this->creacion;
    }

    public function setCreacion(\DateTimeInterface $creacion): self
    {
        $this->creacion = $creacion;

        return $this;
    }

    public function getModificado(): ?\DateTimeInterface
    {
        return $this->modificado;
    }

    public function setModificado(\DateTimeInterface $modificado): self
    {
        $this->modificado = $modificado;

        return $this;
    }

    /**
     * @return Collection|Transaccion[]
     */
    public function getTransaccionRemitente(): Collection
    {
        return $this->transaccion_remitente;
    }

    public function addTransaccionRemitente(Transaccion $transaccionRemitente): self
    {
        if (!$this->transaccion_remitente->contains($transaccionRemitente)) {
            $this->transaccion_remitente[] = $transaccionRemitente;
            $transaccionRemitente->setIdBilleteraRemitente($this);
        }

        return $this;
    }

    public function removeTransaccionRemitente(Transaccion $transaccionRemitente): self
    {
        if ($this->transaccion_remitente->removeElement($transaccionRemitente)) {
            // set the owning side to null (unless already changed)
            if ($transaccionRemitente->getIdBilleteraRemitente() === $this) {
                $transaccionRemitente->setIdBilleteraRemitente(null);
            }
        }

        return $this;
    }
}
