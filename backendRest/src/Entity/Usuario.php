<?php

namespace App\Entity;

use App\Repository\UsuarioRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=UsuarioRepository::class)
 */
class Usuario
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $documento;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nombres;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $celular;

    /**
     * @ORM\OneToMany(targetEntity=Billetera::class, mappedBy="id_usuario")
     */
    private $billeteras;


    public function __construct()
    {
        $this->billeteras = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDocumento(): ?string
    {
        return $this->documento;
    }

    public function setDocumento(string $documento): self
    {
        $this->documento = $documento;

        return $this;
    }

    public function getNombres(): ?string
    {
        return $this->nombres;
    }

    public function setNombres(string $nombres): self
    {
        $this->nombres = $nombres;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getCelular(): ?string
    {
        return $this->celular;
    }

    public function setCelular(string $celular): self
    {
        $this->celular = $celular;

        return $this;
    }

    /**
     * @return Collection|Billetera[]
     */
    public function getBilleteras(): Collection
    {
        return $this->billeteras;
    }

    public function addBilletera(Billetera $billetera): self
    {
        if (!$this->billeteras->contains($billetera)) {
            $this->billeteras[] = $billetera;
            $billetera->setIdUsuario($this);
        }

        return $this;
    }

    public function removeBilletera(Billetera $billetera): self
    {
        if ($this->billeteras->removeElement($billetera)) {
            // set the owning side to null (unless already changed)
            if ($billetera->getIdUsuario() === $this) {
                $billetera->setIdUsuario(null);
            }
        }

        return $this;
    }

}
