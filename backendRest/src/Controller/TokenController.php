<?php

namespace App\Controller;

use Exception;
use App\Entity\Token;
use App\Repository\TokenRepository;
use App\Repository\TransaccionRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

class TokenController extends AbstractController
{
    private $transaccionRepository;

    public function __construct(TokenRepository $tokenRepository, TransaccionRepository $transaccionRepository)
    {
        $this->transaccionRepository = $transaccionRepository;
        $this->tokenRepository = $tokenRepository;
    }

    /**
     * @Route("/token", name="token")
     */
    public function index(): Response
    {
        return $this->json([
            'message' => 'Welcome to your new controller!',
            'path' => 'src/Controller/TokenController.php',
        ]);
    }

    public function add(array $data): Response
    {

        $transaccion = $this->transaccionRepository->find($data[0]['id_transaccion']);

        $token = new Token();
        $token->setIdTransaccion($transaccion);
        $token->setToken($data[0]['token']);
        $token->setStatus(1);//Token valido: 1 - Token no valido: 0

        try {
            $em = $this->getDoctrine()->getManager();
            $em->persist($token);
            $em->flush();

            return new JsonResponse([
                'success' => true,
                'cod_error' => '00',
                'message_error' => false,
                'data' => [
                    'message' => 'Token generado.'
                ]
            ], Response::HTTP_OK);
        }
        catch (Exception $e) {
            return new JsonResponse([
                'success' => false,
                'cod_error' => '04',
                'message_error' => 'Ha ocurrido un problema generando el token.',
                'data' => false
            ], Response::HTTP_OK);
        }
    }

    public function validar(array $data)
    {
        $transaccion = $this->transaccionRepository->find($data[0]['id_transaccion']);

        $token = $this->tokenRepository->findBy(array(
            'id_transaccion' => $transaccion,
            'token' => $data[0]['token'],
        ));

        if(!empty($token)) {
            if ($token[0]->getStatus() == 1) {

                try {

                    $token[0]->setStatus(0);
                    $transaccion->setEstado(0);//confirmada: 0
                    $em = $this->getDoctrine()->getManager();
                    $em->flush();

                    return new JsonResponse([
                        'success' => true,
                        'cod_error' => '00',
                        'message_error' => false,
                        'data' => [
                            'message' => 'Confirmación exitosa.'
                        ]
                    ], Response::HTTP_OK);
                } catch (Exception $e) {
                    return new JsonResponse([
                        'success' => false,
                        'cod_error' => '04',
                        'message_error' => 'Ha ocurrido un problema generando el token.',
                        'data' => false
                    ], Response::HTTP_OK);
                }
                }
            else{
                return new JsonResponse([
                    'success' => false,
                    'cod_error' => '04',
                    'message_error' => 'El token ingresado ya no es valido.',
                    'data' => false
                ], Response::HTTP_OK);
            }
        }
        else {
            return new JsonResponse([
                'success' => false,
                'cod_error' => '04',
                'message_error' => 'El token es incorrecto, por favor verifique el correo nuevamente.',
                'data' => false
            ], Response::HTTP_OK);
        }
    }
}
