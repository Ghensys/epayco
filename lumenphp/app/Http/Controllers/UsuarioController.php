<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Http\Request;

class UsuarioController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function add(Request $request)
    {
        $client = new Client();
        $response = $client->request('POST', 'http://backendrest/index.php/usuario', [
            'form_params' => [
                'documento' => $request->input('documento'),
                'nombres' => $request->input('nombres'),
                'email' => $request->input('email'),
                'celular' => $request->input('celular'),
            ]
        ]);

        return json_decode($response->getBody(), true);
    }
}
