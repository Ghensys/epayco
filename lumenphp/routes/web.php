<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->post('usuario', [
    'as' => 'registrar_usuario', 'uses' => 'UsuarioController@add'
]);

$router->get('billetera/{documento}/{celular}', [
    'as' => 'consultar_saldo', 'uses' => 'BilleteraController@consultarSaldo'
]);

$router->post('billetera/recargar', [
    'as' => 'recarga', 'uses' => 'BilleteraController@recargarBilletera'
]);

$router->post('billetera/pagar', [
    'as' => 'pagar', 'uses' => 'BilleteraController@pagar'
]);

$router->post('billetera/confirmar_pago', [
    'as' => 'confirmar_pago', 'uses' => 'BilleteraController@confirmarPago'
]);