<?php

namespace App\Entity;

use App\Repository\TransaccionRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TransaccionRepository::class)
 */
class Transaccion
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Billetera::class, inversedBy="transaccion_remitente")
     * @ORM\JoinColumn(nullable=false)
     */
    private $id_billetera_remitente;

    /**
     * @ORM\ManyToOne(targetEntity=Billetera::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $id_billetera_destino;

    /**
     * @ORM\Column(type="float")
     */
    private $valor;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $descripcion;

    /**
     * @ORM\Column(type="integer")
     */
    private $estado;

    /**
     * @ORM\Column(type="datetime")
     */
    private $creacion;

    /**
     * @ORM\OneToOne(targetEntity=Token::class, mappedBy="id_transaccion", cascade={"persist", "remove"})
     */
    private $token;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdBilleteraRemitente(): ?Billetera
    {
        return $this->id_billetera_remitente;
    }

    public function setIdBilleteraRemitente(?Billetera $id_billetera_remitente): self
    {
        $this->id_billetera_remitente = $id_billetera_remitente;

        return $this;
    }

    public function getIdBilleteraDestino(): ?Billetera
    {
        return $this->id_billetera_destino;
    }

    public function setIdBilleteraDestino(?Billetera $id_billetera_destino): self
    {
        $this->id_billetera_destino = $id_billetera_destino;

        return $this;
    }

    public function getValor(): ?float
    {
        return $this->valor;
    }

    public function setValor(float $valor): self
    {
        $this->valor = $valor;

        return $this;
    }

    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    public function setDescripcion(?string $descripcion): self
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    public function getEstado(): ?int
    {
        return $this->estado;
    }

    public function setEstado(int $estado): self
    {
        $this->estado = $estado;

        return $this;
    }

    public function getCreacion(): ?\DateTimeInterface
    {
        return $this->creacion;
    }

    public function setCreacion(\DateTimeInterface $creacion): self
    {
        $this->creacion = $creacion;

        return $this;
    }

    public function getToken(): ?Token
    {
        return $this->token;
    }

    public function setToken(Token $token): self
    {
        // set the owning side of the relation if necessary
        if ($token->getIdTransaccion() !== $this) {
            $token->setIdTransaccion($this);
        }

        $this->token = $token;

        return $this;
    }
}
