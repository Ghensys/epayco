<?php

namespace App\Controller;

use App\Entity\Transaccion;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

class TransaccionController extends AbstractController
{
    /**
     * @Route("/transaccion", name="transaccion")
     */
    public function index(): Response
    {
        return $this->json([
            'message' => 'Welcome to your new controller!',
            'path' => 'src/Controller/TransaccionController.php',
        ]);
    }

    public function add(array $data): Response
    {
        $transaccion = new Transaccion();
        $transaccion->setIdBilleteraRemitente($data[0]['remitente']);
        $transaccion->setIdBilleteraDestino($data[0]['destino']);
        $transaccion->setValor($data[0]['valor']);
        $transaccion->setDescripcion($data[0]['tipo_transaccion']);
        $transaccion->setEstado($data[0]['estado']);//1 para indicar satisfactoria
        $transaccion->setCreacion(new \DateTime());

        try {
            $em = $this->getDoctrine()->getManager();
            $em->persist($transaccion);
            $em->flush();

            $this->forward('app.mailer_controller:enviarCorreo', [
                'data' => array([
                    'asunto' => $data[0]['tipo_transaccion'],
                    'message' => $data[0]['mensaje']
                ])
            ]);

            return new JsonResponse([
                'success' => true,
                'cod_error' => '00',
                'message_error' => false,
                'data' => [
                    'codigo_transaccion' => $transaccion->getId(),
                    'message' => 'Su recarga por el valor de '.$data[0]['valor'].' ha sido exitosa.'
                ]
            ], Response::HTTP_OK);
        }
        catch (Exception $e) {
            return new JsonResponse([
                'success' => false,
                'cod_error' => '04',
                'message_error' => 'Ha ocurrido un problema recargando la billetera.',
                'data' => false
            ], Response::HTTP_OK);
        }
    }

    public function validar(array $data): Response
    {
        $data[0]['remitente'];
        $data[0]['remitente'];
        $data[0]['remitente'];
    }
}
